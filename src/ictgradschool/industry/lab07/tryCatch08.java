package ictgradschool.industry.lab07;

/**
 * Created by glee156 on 27/11/2017.
 */
public class tryCatch08 {
    private void throwsClause10() {
        try {
            throws10( null );
            System. out .println( "A" );
        } catch (ArithmeticException e ) {
            System. out .println( e );
        } finally {
            System. out .println( "B" );
        }
        System. out .println( "C" );
    }
    private void throws10(String numS ) throws NullPointerException {
        if ( numS == null ) {
            throw new NullPointerException( "Bad String" );
        }
        System. out .println( "D" );
    }

    public static void main(String[] args) {
        tryCatch08 test = new tryCatch08();
        test.throwsClause10();
    }
}
