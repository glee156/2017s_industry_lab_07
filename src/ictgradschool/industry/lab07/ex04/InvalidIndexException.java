package ictgradschool.industry.lab07.ex04;

/**
 * Created by glee156 on 27/11/2017.
 */
public class InvalidIndexException extends Exception{
    public InvalidIndexException(String message){super(message);};
}
