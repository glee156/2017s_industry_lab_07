package ictgradschool.industry.lab07.ex05;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab07.ex04.InvalidIndexException;

/**
 * TODO Write a program according to the Exercise Five guidelines in your lab handout.
 */
public class ExerciseFive {

    public void start() {
        // TODO Write the codes :)
        //check if text written by user is less than 100 characters and ask to write something shorter if exception is thrown
        String s = "";
        while(true) {
            try {
                s = getUserInput();
                checkLength(s);
                break;
            } catch (ExceedMaxStringLengthException e) {
                System.out.println(e.getMessage());
            }
        }

        //find all words in s and put into array
        String[] allWords = getWords(s);

        //go through each word and check if they are any invalid words
        try{
            checkWord(allWords);
            System.out.println(printFirstLetters(allWords));
        } catch (InvalidWordException e){
            System.out.println("There's an invalid word so no print outs for you");
        }

    }

    // TODO Write some methods to help you.

    public String getUserInput(){
        System.out.print("Please write something fun that's less than 100 characters ");
        return Keyboard.readInput();
    }

    public void checkLength(String s) throws ExceedMaxStringLengthException{
        if(s.length() > 100){
            throw new ExceedMaxStringLengthException("Error: The text is too long");
        }
    }

    public String[] getWords(String s){
        return s.split("\\s+");
    }

    public void checkWord(String[] words) throws InvalidWordException{
        for(int i = 0; i < words.length; i++){
            try{
                Integer.parseInt(Character.toString(words[i].charAt(0)));
                throw new InvalidWordException("Error: not a word");
            } catch (NumberFormatException e){ //catch will execute if the word starts with letter ie. is valid
                continue;
            }
        }
    }

    public String printFirstLetters(String[] allWords){
        String s = "";
        for(int i = 0; i < allWords.length; i++){
            s += Character.toString(allWords[i].charAt(0));
        }
        return s;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseFive().start();
    }
}
