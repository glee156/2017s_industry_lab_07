package ictgradschool.industry.lab07.ex05;

/**
 * Created by glee156 on 27/11/2017.
 */
public class InvalidWordException extends Exception{
    public InvalidWordException(String message){super(message);};
}
