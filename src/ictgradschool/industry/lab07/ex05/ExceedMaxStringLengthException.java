package ictgradschool.industry.lab07.ex05;

/**
 * Created by glee156 on 27/11/2017.
 */
public class ExceedMaxStringLengthException extends Exception {
    public ExceedMaxStringLengthException(String message){super(message);};
}
